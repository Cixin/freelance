<?php

namespace App\Repositories\Users;

use App\Models\User;

interface UsersRepositoryInterface
{
    public function create(array $parameters) : User;
}