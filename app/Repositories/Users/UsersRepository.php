<?php

namespace App\Repositories\Users;

use App\Models\User;

class UsersRepository implements UsersRepositoryInterface
{
    public function create(array $parameters) : User
    {
        return User::create($parameters);
    }
}