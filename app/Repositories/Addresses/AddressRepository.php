<?php

namespace App\Repositories\Addresses;

use App\Models\Address;
use App\Models\User;

class AddressRepository implements AddressesRepositoryInterface
{

    public function create(array $parameters, User $user): Address
    {

        $address = new Address($parameters);

        // If the address is the user's current, set the other addresses to false.
        if ($address->current) {
            $user->addresses()->update(['current' => false]);
        }

        $user->addresses()->save($address);

        return $address;
    }


}