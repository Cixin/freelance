<?php

namespace App\Repositories\Addresses;

use App\Models\Address;
use App\Models\User;

interface AddressesRepositoryInterface
{
    public function create(array $parameters, User $user): Address;
}