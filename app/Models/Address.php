<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['first_line', 'second_line', 'third_line', 'postal_code', 'city', 'country', 'current'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
