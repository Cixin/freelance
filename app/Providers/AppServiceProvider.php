<?php

namespace App\Providers;

use App\Repositories\Addresses\AddressesRepositoryInterface;
use App\Repositories\Addresses\AddressRepository;
use App\Repositories\Users\UsersRepository;
use App\Repositories\Users\UsersRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UsersRepositoryInterface::class, UsersRepository::class);
        $this->app->bind(AddressesRepositoryInterface::class, AddressRepository::class);
    }
}
