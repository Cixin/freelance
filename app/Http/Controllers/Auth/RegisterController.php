<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Repositories\Users\UsersRepositoryInterface;

class RegisterController extends Controller
{
    private $usersRepository;

    public function __construct(UsersRepositoryInterface $repository)
    {
        $this->usersRepository = $repository;
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->validated();
        $data['password'] = bcrypt($data['password']);

        return new UserResource($this->usersRepository->create($data));
    }
}
