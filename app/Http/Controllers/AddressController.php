<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAddressRequest;
use App\Http\Resources\AddressResource;
use App\Models\Address;
use App\Models\User;
use App\Repositories\Addresses\AddressesRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddressController extends Controller
{
    private $addressesRepository;
    /** @var User */
    private $user;

    public function __construct(AddressesRepositoryInterface $repository)
    {
        $this->addressesRepository = $repository;
        $this->user = Auth::user();
    }

    public function index()
    {
        //
    }




    public function store(StoreAddressRequest $request)
    {
        $parameters = $request->validated();

        $address = $this->addressesRepository->create($parameters, $this->user);

        return new AddressResource($address);
    }


    public function show(Address $address)
    {
        //
    }


    public function edit(Address $address)
    {
        //
    }


    public function update(Request $request, Address $address)
    {
        //
    }


    public function destroy(Address $address)
    {
        //
    }
}
