<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AddressResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_line' => $this->first_line,
            'second_line' => $this->second_line,
            'third_line' => $this->third_line,
            'postal_code' => $this->postal_code,
            'city' => $this->city,
            'country' => $this->country,
            'current' => $this->current
        ];
    }
}
