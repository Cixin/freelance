<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_authenticates_the_user()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt('secret')
        ]);

        $this
            ->json('POST', 'api/authenticate', ['email' => $user->email, 'password' => 'secret'])
            ->assertStatus(200)
            ->assertJsonStructure(['access_token', 'token_type', 'expires_in']);
    }

    /** @test */
    public function it_returns_user_information()
    {
        $user = factory(User::class)->create();
        $this
            ->actingAs($user)
            ->json('GET', 'api/me')
            ->assertJson([
                'first_name' => $user->first_name,
                'last_name' => $user->last_name
            ]);
    }
}
