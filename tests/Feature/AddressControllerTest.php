<?php

namespace Tests\Feature;

use App\Models\Address;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddressControllerTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    /** @test */
    public function it_creates_the_address()
    {
        $address = (factory(Address::class)->make())->toArray();

        $this
            ->actingAs($this->user)
            ->json('POST', 'api/addresses', $address);

        $address['user_id'] = $this->user->id;

        $this->assertDatabaseHas('addresses', $address);
    }
}
