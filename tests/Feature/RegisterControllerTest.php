<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_registers_the_user()
    {
        $user = [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'j.doe@example.com',
            'password' => 'secret',
            'password_confirmation' => 'secret'
        ];

        $this
            ->json('POST', 'api/register', $user)
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'id' => 1,
                    'first_name' => $user['first_name'],
                    'last_name' => $user['last_name'],
                    'email' => $user['email']
                ]
            ]);
    }

    /** @test */
    public function email_needs_to_be_unique()
    {
        factory(User::class)->create(['email' => 'j.doe@example.com']);

        $user = [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'j.doe@example.com',
            'password' => 'secret',
            'password_confirmation' => 'secret'
        ];

        $this
            ->json('POST', 'api/register', $user)
            ->assertStatus(422);
    }

}
