<?php

use Faker\Generator as Faker;
use \App\Models\Address;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'first_line' => $faker->name,
        'second_line' => $faker->Address,
        'third_line' => $faker->streetAddress,
        'postal_code' => $faker->postcode,
        'city' => $faker->city,
        'country' => $faker->country,
        'current' => $faker->boolean
    ];
});
